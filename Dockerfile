# Stage 1

FROM node:16.13.0 AS builder

WORKDIR /app

COPY package.json package-lock.json .

RUN npm install 

ENV PATH="./node_modules/.bin:$PATH"

COPY . .

RUN ng build --prod

CMD ["npm", "start"]

# Stage 2
FROM nginx:1.13.12-alpine

COPY --from=node /usr/src/app/dist /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'